%undefine __cmake_in_source_build

Name:           jmscontester
Version:        1.0.0
Release:        4%{?dist}
Summary:        Tool to test JMS connection

License:        BSD
URL:            https://gitlab.com/profidata-ag/tools/%{name}
Source0:        https://gitlab.com/profidata-ag/tools/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

%if 0%{?rhel}
BuildRequires:  gcc-toolset-10
%else
BuildRequires:  gcc-c++
%endif
BuildRequires:  cmake
BuildRequires:  activemq-cpp-devel

%description
Tool to test JMS connection

%prep
%autosetup

%build
%if 0%{?rhel}
scl enable gcc-toolset-10 - << \SCLEOF
set -e
%endif
%cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo
%cmake_build
%if 0%{?rhel}
SCLEOF
%endif

%install
%cmake_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}

%changelog
* Sat Jun 26 2021 Levent Demirörs <levent.demiroers@profidata.com> - 1.0.0-4
- Fix cmake requirement and macros
- Add changelog

* Sat Jun 26 2021 Levent Demirörs <levent.demiroers@profidata.com> - 1.0.0-3
- Upgrade to GCC 10
